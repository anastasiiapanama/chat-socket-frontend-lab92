
const initialState = {
    messages: [],
    activeUsers: [],
    messagesLoading: false
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'NEW_MESSAGES':
            return {...state, messages: action.messages};
        case 'ACTIVE_USERS':
            return {...state, activeUsers: action.users};
        default:
            return state;
    };
};

export default messagesReducer;