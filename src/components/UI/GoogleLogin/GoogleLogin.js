import React from 'react';
import GoogleLoginButton from 'react-google-login';
import Button from "@material-ui/core/Button";
import GoogleIcon from "../GoogleIcon/GoogleIcon";
import {googleClientId} from "../../../config";
import {useDispatch} from "react-redux";
import {googleLogin} from "../../../store/actions/usersActions";

const GoogleLogin = () => {
    const dispatch = useDispatch();

    const handleLogin = response => {
        dispatch(googleLogin(response));
    };

    return (
        <GoogleLoginButton
            clientId={googleClientId}
            render={props => (
                <Button
                    fullWidth
                    color="primary"
                    variant="outlined"
                    startIcon={<GoogleIcon/>}
                    onClick={props.onClick}
                >
                    Login with Google
                </Button>
            )}
            onSuccess={handleLogin}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLogin;