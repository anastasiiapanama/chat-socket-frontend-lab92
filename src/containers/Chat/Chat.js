import React, {useEffect, useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {
    Divider,
    List,
    ListItem,
    ListItemText,
    Paper,
    TextField
} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import Fab from "@material-ui/core/Fab";
import SendIcon from "@material-ui/icons/Send";
import Post from "../../components/Post/Post";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    chatSection: {
        width: '100%',
        height: '100vh'
    },
    headBG: {
        backgroundColor: '#e0e0e0'
    },
    borderRight500: {
        borderRight: '1px solid #e0e0e0'
    },
    messageArea: {
        height: '70vh',
        overflowY: 'auto'
    },
    progress: {
        height: 200
    },
    form: {
        flexDirection: 'row'
    },
    textField: {
        width: '85%'
    },
    formButton: {
        marginLeft: '15px'
    }
});

const Chat = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const messages = useSelector(state => state.messages.messages);
    const activeUsers = useSelector(state => state.messages.activeUsers);
    const [message, setMessage] = useState('');
    const ws = useRef(null);

    useEffect(() => {
        const url = 'ws://localhost:8000/chat';
        ws.current = new WebSocket(user ? `${url}?token=${user.token}` : url);

        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            dispatch(decoded);
        }
    }, [dispatch]);

    const sendMessage = () => {
        ws.current.send(JSON.stringify({type: 'CREATE_MESSAGE', message, user}));
        setMessage('');
    };

    return (
        <>
            {user?.role === 'user' ? (
                <Grid container component={Paper} className={classes.chatSection}>
                    <Grid item xs={3} className={classes.borderRight500}>
                        <List>
                            <ListItem button key="Anastasiia">
                                <ListItemText primary="Chat room">Chat room</ListItemText>
                            </ListItem>
                        </List>
                        <Divider/>
                        <Grid item xs={12} style={{padding: '10px'}}>
                            <TextField id="outlined-basic-email" label="Search" variant="outlined" fullWidth/>
                        </Grid>
                        <Divider/>
                        <List>
                            <ListItem button key="Anastasiia">
                                <ListItemText primary="Online users">Online users</ListItemText>
                            </ListItem>
                            {activeUsers && (
                                <>
                                    {activeUsers.map(user => (
                                        <ListItem button key={user._id}>
                                            <ListItemText>{user.displayName}</ListItemText>
                                        </ListItem>
                                    ))}
                                </>
                            )}
                        </List>
                    </Grid>
                    <Grid item xs={9}>
                        <List className={classes.messageArea}>
                            {messages.map(message => {
                                return (
                                    <Post
                                        key={message._id}
                                        message={message.message}
                                        author={message.user.displayName}
                                        datetime={message.datetime}
                                    />
                                )
                            })}
                        </List>
                        <Divider/>
                        <Grid style={{padding: '20px'}}>
                            <TextField label="Type Something" className={classes.textField}
                               required
                               name="message" type="text" value={message}
                               onChange={e => setMessage(e.target.value)}
                               fullWidth
                            />
                            <Fab color="primary" onClick={sendMessage} aria-label="add" className={classes.formButton}>
                                <SendIcon/>
                            </Fab>
                        </Grid>
                    </Grid>
                </Grid>
            ) : (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <Typography>Sign in to start messaging</Typography>
                    </Grid>
                </Grid>
            )}
        </>
    );
};

export default Chat;